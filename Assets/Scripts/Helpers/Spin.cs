﻿using UnityEngine;

namespace CoupleGame
{
    public class Spin : MonoBehaviour
    {
        public float AngularVelocity;

        private void Update()
        {
            transform.Rotate(Vector3.back, AngularVelocity * Time.deltaTime);
        }
    }
}