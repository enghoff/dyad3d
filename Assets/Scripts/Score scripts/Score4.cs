﻿using UnityEngine;

public class Score4 : Score
{
    public GameObject prisonbutton;
    public string selfish = "NEGATIVE";

    public float search0;
    public float search1;
    public float search2;
    public float revenge;

    public float p1cubetime;
    public float p2cubetime;
    public GameObject p1goldcube;
    public GameObject p2goldcube;

    private new void Update()
    {
        base.Update();

        search0 = _shortcuttoP1score.hesitation;
        search1 = _shortcuttoP1score.primarySearch;
        search2 = _shortcuttoP1score.secondarySearch;

        SelfishTest();
        RevengeTest();
    }

    void SelfishTest()
    {
        if (P1score >= 1 && finalprison.activeInHierarchy == true)
        {
            selfish = "Prioritised self";
        }
        else if (P1score <= 0 && finalprison.activeInHierarchy == false)
        {
            selfish = "altruistic";
        }
    }

    void RevengeTest()
    {
        if (prisonbutton.activeInHierarchy == true && finalprison.activeInHierarchy == true)
        {
            revenge += Time.deltaTime * 1f;
        }
    }

    void P1cubetimertest()
    {
        if (p1goldcube.activeInHierarchy == true)
        {
            p1cubetime += Time.deltaTime * 1f;
        }
    }

    protected override void SetTotalScore()
    {
        base.SetTotalScore();

        if (overallScore >= 2)
        {
            Stats.SetFloat("Level4Score", levelscore);
            Stats.SetFloat("MiddleMapHesitation_Level4", search0);
            Stats.SetFloat("PrimarySearchZoneDuration_Level4", search1);
            Stats.SetFloat("SecondarySearchZoneDuration_Level4", search2);
            Stats.SetString("Priorities_Level4", selfish);
            Stats.SetFloat("UnlockingPrisonToFreeingFromPrisonTime_Level4", revenge);
            Stats.SetFloat("p1cubetime", p1cubetime);
        }
    }
}