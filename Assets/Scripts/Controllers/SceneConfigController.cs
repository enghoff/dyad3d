﻿using UnityEngine;

using System.Linq;
using System.Text.RegularExpressions;

public class SceneConfigController : MonoBehaviour
{
    public SceneConfig Prefab;
    public Transform Content;
    public SceneField[] SceneField;

    private string Format(string input)
    {
        return Regex.Replace(input, "[A-Z]", " $0");
    }

    private void OnEnable()
    {
        foreach (var scene in GetComponentsInChildren<SceneConfig>()) Destroy(scene.gameObject);
        foreach (var scene in SceneField)
        {
            var instance = Instantiate(Prefab, Content);
            var match = Config.config.Scenes.FirstOrDefault(s => s.Name == scene.SceneName);

            instance.Title.text = Format(scene.SceneName);
            instance.name = scene.SceneName;
            instance.Active.isOn = (match != null) ? match.Active : true;
            instance.Active.onValueChanged.AddListener((include) => 
            {
                Config.config.Scenes = GetComponentsInChildren<SceneConfig>().Select(s => new Model.Scene()
                {
                    Name = s.name,
                    Active = s.Active.isOn
                }).ToList();

                Config.Commit(false);
            });
        }
    }
}
