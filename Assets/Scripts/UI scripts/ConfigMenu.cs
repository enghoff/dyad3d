﻿using UnityEngine;
using UnityEngine.UI;

public class ConfigMenu : MonoBehaviour
{
    public string BaseURL;

    public InputField AIinputID;
	public InputField AIdesc;
    public Toggle DynamicComms;
	public Toggle DisplayInfoInGame;
	public Toggle DisplayOnlineInfo;
    public InputField AIinput1;
	public InputField AIinput2;
	public InputField AIinput3;
	public InputField AIinput4;
	public InputField AI1typedelay;
	public InputField AI2typedelay;
	public InputField AI3typedelay;
	public InputField AI4typedelay;
	public InputField UsersOnline;
	public InputField InactiveUsers;
	public InputField GamesFinished;
	public InputField HighScore;
	public InputField Intro1;
	public InputField Intro2;
	public InputField ID_Dyad1A;
	public InputField ID_Dyad1B;
	public InputField ID_Dyad2A;
	public InputField ID_Dyad2B;
	public InputField ID_Dyad3A;
	public InputField ID_Dyad3B;
	public InputField ID_Dyad4A;
	public InputField ID_Dyad4B;
	public InputField ID_Dyad5A;
	public InputField ID_Dyad5B;
	public InputField ID_Dyad6A;
	public InputField ID_Dyad6B;
	public InputField ID_Dyad7A;
	public InputField ID_Dyad7B;
	public InputField ID_Dyad8A;
	public InputField ID_Dyad8B;
	public InputField ID_Dyad9A;
	public InputField ID_Dyad9B;
    public Text GameURL;

    private void Start()
    {
        PopulateConfiguration();
    }

    public void CommitConfiguration()
    {
        Config.config.AIID = AIinputID.text;
        Config.config.AIDesc = AIdesc.text;
        Config.config.DynamicComms = DynamicComms.isOn;
        Config.config.DisplayInfoInGame = DisplayInfoInGame.isOn;
        Config.config.DisplayOnlineInfo = DisplayOnlineInfo.isOn;
        Config.config.AImessage1 = AIinput1.text;
        Config.config.AImessage2 = AIinput2.text;
        Config.config.AImessage3 = AIinput3.text;
        Config.config.AImessage4 = AIinput4.text;
        Config.config.AI1typedelay = int.Parse(AI1typedelay.text);
        Config.config.AI2typedelay = int.Parse(AI2typedelay.text);
        Config.config.AI3typedelay = int.Parse(AI3typedelay.text);
        Config.config.AI4typedelay = int.Parse(AI4typedelay.text);
        Config.config.UO = int.Parse(UsersOnline.text);
        Config.config.IU = int.Parse(InactiveUsers.text);
        Config.config.GF = int.Parse(GamesFinished.text);
        Config.config.HS = int.Parse(HighScore.text);
        Config.config.Intro1 = Intro1.text;
        Config.config.Intro2 = Intro2.text;
        Config.config.Dyad1A = ID_Dyad1A.text;
        Config.config.Dyad1B = ID_Dyad1B.text;
        Config.config.Dyad2A = ID_Dyad2A.text;
        Config.config.Dyad2B = ID_Dyad2B.text;
        Config.config.Dyad3A = ID_Dyad3A.text;
        Config.config.Dyad3B = ID_Dyad3B.text;
        Config.config.Dyad4A = ID_Dyad4A.text;
        Config.config.Dyad4B = ID_Dyad4B.text;
        Config.config.Dyad5A = ID_Dyad5A.text;
        Config.config.Dyad5B = ID_Dyad5B.text;
        Config.config.Dyad6A = ID_Dyad6A.text;
        Config.config.Dyad6B = ID_Dyad6B.text;
        Config.config.Dyad7A = ID_Dyad7A.text;
        Config.config.Dyad7B = ID_Dyad7B.text;
        Config.config.Dyad8A = ID_Dyad8A.text;
        Config.config.Dyad8B = ID_Dyad8B.text;
        Config.config.Dyad9A = ID_Dyad9A.text;
        Config.config.Dyad9B = ID_Dyad9B.text;

        Config.Commit();
    }

    public void PopulateConfiguration()
	{
        AIinputID.text = Config.config.AIID;
        AIdesc.text = Config.config.AIDesc;
        DynamicComms.isOn = Config.config.DynamicComms;
        DisplayInfoInGame.isOn = Config.config.DisplayInfoInGame;
        DisplayOnlineInfo.isOn = Config.config.DisplayOnlineInfo;
        AIinput1.text = Config.config.AImessage1;
		AIinput2.text = Config.config.AImessage2;
        AIinput3.text = Config.config.AImessage3;
        AIinput4.text = Config.config.AImessage4;
        AI1typedelay.text = Config.config.AI1typedelay.ToString();
        AI2typedelay.text = Config.config.AI2typedelay.ToString();
        AI3typedelay.text = Config.config.AI3typedelay.ToString();
        AI4typedelay.text = Config.config.AI4typedelay.ToString();
        UsersOnline.text = Config.config.UO.ToString();
        InactiveUsers.text = Config.config.IU.ToString();
        GamesFinished.text = Config.config.GF.ToString();
        HighScore.text = Config.config.HS.ToString();
        Intro1.text = Config.config.Intro1;
        Intro2.text = Config.config.Intro2;
        ID_Dyad1A.text = Config.config.Dyad1A;
        ID_Dyad1B.text = Config.config.Dyad1B;
        ID_Dyad2A.text = Config.config.Dyad2A;
        ID_Dyad2B.text = Config.config.Dyad2B;
		ID_Dyad3A.text = Config.config.Dyad3A;
		ID_Dyad3B.text = Config.config.Dyad3B;
		ID_Dyad4A.text = Config.config.Dyad4A;
		ID_Dyad4B.text = Config.config.Dyad4B;
		ID_Dyad5A.text = Config.config.Dyad5A;
		ID_Dyad5B.text = Config.config.Dyad5B;
		ID_Dyad6A.text = Config.config.Dyad6A;
		ID_Dyad6B.text = Config.config.Dyad6B;
		ID_Dyad7A.text = Config.config.Dyad7A;
		ID_Dyad7B.text = Config.config.Dyad7B;
		ID_Dyad8A.text = Config.config.Dyad8A;
		ID_Dyad8B.text = Config.config.Dyad8B;
		ID_Dyad9A.text = Config.config.Dyad9A;
		ID_Dyad9B.text = Config.config.Dyad9B;

        if (Application.platform == RuntimePlatform.WebGLPlayer) GameURL.text = BaseURL + "?" + Config.user.Guid;
    }

    public void LoadDefaultSettings()
	{
		Config.config.AIID = "59ec42713e70330001cf63e2";
		Config.config.AIDesc = "i found balancing multiple tasks tricky but my control is good";
		Config.config.Intro1 = "Dyad3D is a collaborative game. \n\nThe game will involve navigating through a maze with a randomly selected online partner. \n\nClick NEXT to start the tutorial to familiarise yourself with the game.";
		Config.config.Intro2 = "You will be paired randomly with another online partner.\n\nYou will NOT be able to communicate with your partner, but you can send information about how easy/hard it was to complete the tutorial.\n\nThis may help your partner to understand how you will collaborate together in the game.";
        Config.config.AI1typedelay = 3;
        Config.config.AI2typedelay = 5;
        Config.config.AI3typedelay = 9;
        Config.config.AI4typedelay = 8;
        Config.config.UO = 18;
        Config.config.IU = 1;
        Config.config.GF = 1062;
        Config.config.HS = 2148;
		Config.config.Dyad1A = "59e0c9f862637600014a6864";
		Config.config.Dyad1B = "5827c9fc369d5c000171d711";
		Config.config.Dyad2A = "5ab678d1e1846900019b5f74";
		Config.config.Dyad2B = "599c9d5dd0937e00014923f0";
		Config.config.Dyad3A = "5c29a517198b750001c75a5b";
		Config.config.Dyad3B = "5ae84ac8d28d2f0001023645";
		Config.config.Dyad4A = "596482fc5f170a00014358cc";
		Config.config.Dyad4B = "5b22b532113fe900016b0b67";
		Config.config.Dyad5A = "5a522c0713748300014b0ee4";
		Config.config.Dyad5B = "59a1be88c458e800017c577e";
		Config.config.Dyad6A = "5b631ebb60c40e000103e5ce";
		Config.config.Dyad6B = "59ae43b13e4b5a00016aa9d9";
		Config.config.Dyad7A = "58c115298a3f870001c0e70e";
		Config.config.Dyad7B = "5b223ee733648300014df654";
		Config.config.Dyad8A = "58bc3369c458e8000237e544";
		Config.config.Dyad8B = "5b631ebe654a220001556efc";
		Config.config.Dyad9A = "5a64432eae4b5a00015ec311";
		Config.config.Dyad9B = "58c5efa900c5370001eaa7c0";

        PopulateConfiguration();
        CommitConfiguration();
    }

    public void Load()
    {
        Config.Load();
        PopulateConfiguration();
        CommitConfiguration();
    }

    public void Save()
    {
        CommitConfiguration();
        Config.Save();
    }
}
