﻿using UnityEngine;
using UnityEngine.UI;

public class LoadAIdesc : MonoBehaviour
{
	public Text AIdesc;
	public string x;

	// Use this for initialization
	void Start ()
    {
		x = Config.config.AIDesc.FormatDefault("failed to load");

		AIdesc.text = "" + (x);
	}
}
