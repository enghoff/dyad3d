﻿using UnityEngine;

public class LeaderboardScript : MonoBehaviour
{
    public GameObject AIFeedback;
	public GameObject PlayerFeedback;
	public GameObject NextButton;

    private void Awake()
    {
        if (AIFeedback != null) AIFeedback.SetActive(Config.config.DynamicComms);
		if (PlayerFeedback != null) PlayerFeedback.SetActive (Config.config.DynamicComms);
		if (NextButton != null) NextButton.SetActive(!Config.config.DynamicComms);
    }
}