﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour
{
    //Toggle game objects
    public Toggle isWellbeing;
    public GameObject isWellbeingChild;
    public Toggle isEconomicStatus;
    public GameObject isEconomicStatusChild;
    public Toggle isReligion;
    public GameObject isReligionChild;
    public Toggle isNationality;
    public GameObject isNationalityChild;
    public Toggle isLevelofEducation;
    public GameObject isLevelofEducationChild;

    //checks active toggle

    public void ActiveToggle()
    {
        if (isWellbeing.isOn)
        {
            isWellbeingChild.SetActive(true);
        }

        else if (isEconomicStatus.isOn)
        {
            isEconomicStatusChild.SetActive(true);
        }

        else if (isReligion.isOn)
        {
            isReligionChild.SetActive(true);
        }

        else if (isNationality.isOn)
        {
            isNationalityChild.SetActive(true);
        }

        else if (isLevelofEducation.isOn)
        {
            isLevelofEducationChild.SetActive(true);
        }
    }

    public void OnSubmit()
    {
        ActiveToggle();
    }
}
