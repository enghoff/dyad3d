﻿using UnityEngine;

public class Score3 : Score
{
    protected override void SetTotalScore()
    {
        base.SetTotalScore();

        if (overallScore >= 2) Stats.SetFloat("Level3Score", levelscore);
	}
}