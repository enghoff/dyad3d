﻿using UnityEngine;
using UnityEngine.UI;

//This script is loads the AI identification number set in the original script, on window 1, 'PlayerName'. 

public class Load_AI_name : MonoBehaviour
{
    public Text AI_Text;
    public string AI_ID;

    void Start()
    {
        AI_ID = Config.config.AIID.FormatDefault("failed to load");
        AI_Text.text = "Player " + (AI_ID);
    }
}
