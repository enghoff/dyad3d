﻿using UnityEngine;

//This is the script that allows the AI to open doors and prisons

public class OpenByAI : MonoBehaviour
{
    public GameObject door;

    void Start()
    {
        door.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "AI")
        {
            door.gameObject.SetActive(false);

        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "AI")
        {
            door.gameObject.SetActive(false);
        }
    }
}


