﻿using UnityEngine;

[ExecuteInEditMode]
public class ConditionalWebGL : MonoBehaviour
{
    public GameObject[] WebGL;
    public GameObject[] Standalone;

    private void Update ()
    {
#if UNITY_WEBGL
        foreach (var go in WebGL) go.SetActive(true);
        foreach (var go in Standalone) go.SetActive(false);
#elif UNITY_STANDALONE
        foreach (var go in WebGL) go.SetActive(false);
        foreach (var go in Standalone) go.SetActive(true);
#endif
    }
}
