﻿using UnityEngine;
using UnityEngine.UI;

public class Load_player_ID : MonoBehaviour
{
    public Text Playername;
    public string x;

    // Use this for initialization
    void Start()
    {
        x = Stats.GetString("PlayerName", "failed to load");

        Playername.text = "" + (x);
    }
}
