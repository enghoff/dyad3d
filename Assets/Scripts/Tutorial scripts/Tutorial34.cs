﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class Tutorial34 : MonoBehaviour
{
    PlayerController _shortcuttoP1score;
    PracticeAIcontroller _shortcuttoAIscore;
    public int overallScore;
    public Text totalScore;
    //    public Text winText;
    public int P1score;
    public int AIscore;
    public GameObject NextLevel;
    public GameObject AIplayer;
    public GameObject Playerplayer;
    public GameObject PlayerBlast;
    public GameObject AIBlast;

    public int delay = 2;

    void Start()
    {
        _shortcuttoP1score = gameObject.GetComponentInChildren<PlayerController>();
        _shortcuttoAIscore = gameObject.GetComponentInChildren<PracticeAIcontroller>();

        SetTotalScore();
        NextLevel.SetActive(false);
        PlayerBlast.SetActive(false);
        AIBlast.SetActive(false);
    }

    void Update()
    {
        P1score = _shortcuttoP1score.count;
        AIscore = _shortcuttoAIscore.count;
        overallScore = P1score + AIscore;
        SetTotalScore();
    }

    void SetTotalScore()
    {
        totalScore.text = "Total Score: " + overallScore.ToString();
        if (overallScore >= 2)
        {
            StartCoroutine(EndGame());
        }
    }

    IEnumerator EndGame()
    {
        PlayerBlast.SetActive(true);
        AIBlast.SetActive(true);
        yield return new WaitForSecondsRealtime(delay);
        AIplayer.SetActive(false);
        Playerplayer.SetActive(false);
        NextLevel.SetActive(true);
    }
}