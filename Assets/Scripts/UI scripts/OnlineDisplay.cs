﻿using UnityEngine;
using UnityEngine.UI;

public class OnlineDisplay : MonoBehaviour
{
	public GameObject OnlineDisplayPanel;

	public Text UOtext;
	public Text IUtext;
	public Text GFtext;
	public Text HStext;

	public int UOint;
	public int IUint;
	public int GFint;
	public int HSint;

	private void Awake()
	{
		if (OnlineDisplayPanel != null) OnlineDisplayPanel.SetActive(Config.config.DisplayOnlineInfo);

		UOint = Config.config.UO;
		UOtext.text = UOint.ToString();

		IUint = Config.config.IU;
		IUtext.text = IUint.ToString();

		GFint = Config.config.GF;
		GFtext.text = GFint.ToString();

		HSint = Config.config.HS;
		HStext.text = HSint.ToString();
	}
}