﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

//This script is designed to further the illusion that participants are playing online with someone.
//This script makes it looks like the 'other person' selects a grey colour. The script waits one second before shading in the grey colour ball option.
//It forces the participant to be a red colou ball. 

public class Force_colour_script : MonoBehaviour
{
    public GameObject ShadedPanel;
    public GameObject AIID;
    public Text buttontext;

    // Use this for initialization
    void Start()
    {
        ShadedPanel.SetActive(false);
        AIID.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(HidePanel());
    }

    IEnumerator HidePanel()
    {
        yield return new WaitForSecondsRealtime(1);     //This is the coroutine where the script waits for one second before shading out the grey ball option.
        ShadedPanel.SetActive(true);
        AIID.SetActive(true);
        buttontext.text = "Selected";
    }
}
