﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardScript3 : LeaderboardScript
{
    public Text L3score;
    public float L3finalscore;
    public Text AImessageL3;
    public string AImessage;
    public int typingdelayL3;
    public string PlayerMessage3;
    public InputField PlayerInputMessage3;
    public GameObject Tick;

    // Use this for initialization
    void Start()
    {
        L3finalscore = Stats.GetFloat("Level3Score");
        L3score.text = "" + (L3finalscore);
        typingdelayL3 = Config.config.AI3typedelay;
        StartCoroutine(TypingDelay());
    }

    public void SavePlayerMessage3()
    {
        PlayerMessage3 = PlayerInputMessage3.text;
        Stats.SetString("PlayerMessage3", PlayerMessage3);
    }

    IEnumerator TypingDelay()
    {
        yield return new WaitForSecondsRealtime(typingdelayL3);
        Debug.Log("coroutine typing delay active");
        AImessage = Config.config.AImessage3;
        Tick.SetActive(true);
        AImessageL3.text = (AImessage);
    }
}