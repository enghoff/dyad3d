﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

public class OpenURL : MonoBehaviour, IPointerDownHandler
{
    public string BaseURL;

    [DllImport("__Internal")]
    private static extern void openWindow(string url);

    public void OnPointerDown(PointerEventData eventData)
    {
#if !UNITY_EDITOR
		openWindow(string.Format(BaseURL, Config.user.Guid));
#endif
	}
}