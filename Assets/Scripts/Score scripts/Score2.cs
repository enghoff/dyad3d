﻿using UnityEngine;

public class Score2 : Score
{
    protected override void SetTotalScore()
    {
        base.SetTotalScore();

        if (overallScore >= 2) Stats.SetFloat("Level2Score", levelscore);
	}
}