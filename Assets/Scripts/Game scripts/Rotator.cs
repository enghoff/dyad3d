﻿using UnityEngine;
using System.Collections;

//This script rotates the gold cubes at the end of the level to make them look like objects to collect.

public class Rotator : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}