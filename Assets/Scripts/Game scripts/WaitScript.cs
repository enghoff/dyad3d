﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This is a small script only attached to the AI in level 1. 
//It is designed to simulate the effect of hesitation from the computer, otherwise it looks suspicious if the AI immediately knows what to do in the game.

public class WaitScript : MonoBehaviour
{
	public bool freedom;

    private Patrol _turnoffnav;
    private int _counter = 0;
    private int _delay = 1;

	// Use this for initialization
	void Start ()
    {
		_turnoffnav = gameObject.GetComponentInChildren<Patrol> ();
		_turnoffnav.enabled = !_turnoffnav.enabled;
		freedom = false;		
	}
	
	void Update ()
    {
		if (_counter <= 1)
        {
			StartCoroutine (Breather());
			_counter++;
		}
	}

	IEnumerator Breather()
	{
		yield return new WaitForSecondsRealtime (_delay);
		Debug.Log ("coroutine breather active");
		_turnoffnav.enabled = true;
	}
}
