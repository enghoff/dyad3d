﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

using System.Collections;

public class Score : MonoBehaviour
{
    public Text winText;
    public Text totalScore;
    public int overallScore;
    public int P1score;
    public int AIscore;
    public GameObject finalprison;
    public GameObject NextLevel;
    public GameObject AIplayer;
    public GameObject Playerplayer;
    public GameObject PlayerBlast;
    public GameObject AIBlast;
    public Text counterText;
    public float timeLeft = 1000.0f;
    public float levelscore;
    public float seconds, minutes;
    public Color ten;
    public Color nine;
    public Color eight;
    public Color seven;
    public Color six;
    public Color five;
    public Color four;
    public Color three;
    public Color two;
    public Color one;
    public Color dead;

    public int delay = 2;
    public GameObject descAI;
	public GameObject descPlayer;

    protected NavMeshAgent _navMeshAgent;
    protected PlayerController _shortcuttoP1score;
    protected AIController _shortcuttoAIscore;

    protected void Start()
    {
        _shortcuttoP1score = gameObject.GetComponentInChildren<PlayerController>();
        _shortcuttoAIscore = gameObject.GetComponentInChildren<AIController>();
        _navMeshAgent = gameObject.GetComponentInChildren<NavMeshAgent>();

        if (_navMeshAgent != null) _navMeshAgent.enabled = false;
        if (winText != null) winText.text = "";

        if (descAI != null) descAI.SetActive(Config.config.DisplayInfoInGame);
        if (descPlayer != null) descPlayer.SetActive(Config.config.DisplayInfoInGame);

        SetTotalScore();
        NextLevel.SetActive(false);
        PlayerBlast.SetActive(false);
        AIBlast.SetActive(false);
    }

    protected void Update()
    {
        P1score = _shortcuttoP1score.count;
        AIscore = _shortcuttoAIscore.collisionCount;
        overallScore = P1score + AIscore;
        SetTotalScore();

        if (_navMeshAgent != null && (finalprison == null || !finalprison.activeInHierarchy))
        {
            _navMeshAgent.enabled = true;
        }
    }

    protected virtual void SetTotalScore()
    {
        if (totalScore != null) totalScore.text = "Total Score: " + overallScore.ToString();
        if (overallScore < 2)
        {
            timeLeft -= Time.deltaTime * 8f;

            counterText.text = "Score: " + Mathf.Round(timeLeft);

            if (timeLeft >= 900) counterText.color = ten;
            else if (timeLeft >= 800) counterText.color = nine;
            else if (timeLeft >= 700) counterText.color = eight;
            else if (timeLeft >= 600) counterText.color = seven;
            else if (timeLeft >= 500) counterText.color = six;
            else if (timeLeft >= 400) counterText.color = five;
            else if (timeLeft >= 300) counterText.color = four;
            else if (timeLeft >= 200) counterText.color = three;
            else if (timeLeft >= 100) counterText.color = two;
            else if (timeLeft > 0) counterText.color = one;
            else if (timeLeft <= 0) counterText.color = dead;
        }
        else if (overallScore >= 2)
        {
            levelscore = Mathf.Round(timeLeft);
            if (winText != null) winText.text = "" + (levelscore);
            StartCoroutine(EndGame());
        }
    }

    protected IEnumerator EndGame()
    {
        PlayerBlast.SetActive(true);
        AIBlast.SetActive(true);
        yield return new WaitForSecondsRealtime(delay);
        AIplayer.SetActive(false);
        Playerplayer.SetActive(false);
        NextLevel.SetActive(true);
    }
}
