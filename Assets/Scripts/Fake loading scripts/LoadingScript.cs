﻿using UnityEngine;

public class LoadingScript : MonoBehaviour
{
    public GameObject Nextbutton;
    public int wait = 14;
    public GameObject Loadingeffect;
    public GameObject Waitingtext;
    public GameObject Readytext;

    public float seconds, minutes;

    // Use this for initialization
    void Start()
    {
        Nextbutton.SetActive(false);
        Readytext.SetActive(false);
        wait = 14;
    }

    // Update is called once per frame
    void Update()
    {
        minutes = (int)(Time.timeSinceLevelLoad / 60f);
        seconds = (int)(Time.timeSinceLevelLoad % 60f);
        if (seconds > wait)
        {
            Nextbutton.SetActive(true);
            Readytext.SetActive(true);
            Loadingeffect.SetActive(false);
            Waitingtext.SetActive(false);
        }
        else if (seconds <= wait)
        {
            Nextbutton.SetActive(false);
            Readytext.SetActive(false);
            Loadingeffect.SetActive(true);
            Waitingtext.SetActive(true);
        }
    }
}
