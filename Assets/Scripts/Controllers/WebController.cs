﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

using System;
using System.Net;
using System.Text;
using System.Collections;

using LitJson;

public class WebController : MonoBehaviour
{
    public UnityEvent OnCommence;
    public UnityEvent OnComplete;
    public UnityEvent OnNetworkError;

    private struct Query
    {
        public string Path;
        public string Data;
        public string Method;
        public bool ShowSpinner;
        public UnityAction<string> OnSuccess;
        public UnityAction<long> OnError;
    }

    private static readonly string URL = "https://dyad3d.ipocresearch.co.uk";

    private static Query _errorQuery;
    private static WebController _instance;

    private void Awake()
    {
        _instance = this;
    }

    public static string ErrorString(long responseCode)
    {
        return responseCode + ": " + (HttpStatusCode)responseCode;
    }

    public static void Post<T0, T1>(string path, T0 obj, UnityAction<T1> onSuccess = null, UnityAction<long> onError = null, bool showSpinner = true)
    {
        var query = new Query()
        {
            Path = path,
            Data = JsonMapper.ToJson(obj),
            Method = UnityWebRequest.kHttpVerbPOST,
            OnSuccess = data => { if (onSuccess != null) onSuccess(JsonMapper.ToObject<T1>(data)); },
            OnError = code => { if (onError != null) onError(code); },
            ShowSpinner = showSpinner
        };

        _instance.StartCoroutine(_instance.Request(query));
    }

    public static void Get<T>(string path, UnityAction<T> onSuccess = null, UnityAction<long> onError = null, bool showSpinner = true)
    {
        var query = new Query()
        {
            Path = path,
            Method = UnityWebRequest.kHttpVerbGET,
            OnSuccess = data => { if (onSuccess != null) onSuccess(JsonMapper.ToObject<T>(data)); },
            OnError = code => { if (onError != null) onError(code); },
            ShowSpinner = showSpinner
        };

        _instance.StartCoroutine(_instance.Request(query));
    }

    private void OnError(UnityEvent onError, Query query)
    {
        _errorQuery = query;

        onError.Invoke();
    }

    public void Retry()
    {
        _instance.StartCoroutine(_instance.Request(_errorQuery));
    }

    private UnityWebRequest JSONWebRequest(string URL, string data)
    {
        var request = new UnityWebRequest(URL, UnityWebRequest.kHttpVerbPOST);

        Debug.Log(URL + Environment.NewLine + data);

        request.SetRequestHeader(@"Content-Type", @"application/json");
        request.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(data));
        request.downloadHandler = new DownloadHandlerBuffer();

        return request;
    }

    private IEnumerator Request(Query query)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable) OnError(OnNetworkError, query);
        else
        {
            if (query.ShowSpinner) OnCommence.Invoke();

            using (var request = (query.Method == UnityWebRequest.kHttpVerbGET) ? UnityWebRequest.Get(URL + query.Path) : JSONWebRequest(URL + query.Path, query.Data))
            {
                yield return request.SendWebRequest();

                if (query.ShowSpinner) OnComplete.Invoke();

                if (request.isNetworkError) OnError(OnNetworkError, query);
                else if (request.responseCode >= 300)
                {
                    Debug.Log(query.Method + " " + URL + query.Path + Environment.NewLine + ErrorString(request.responseCode) + Environment.NewLine + request.downloadHandler.text);

                    if (request.responseCode == 500) OnError(OnNetworkError, query);
                    else query.OnError(request.responseCode);
                }
                else
                {
                    Debug.Log(query.Method + " " + URL + query.Path + Environment.NewLine + ErrorString(request.responseCode) + Environment.NewLine + request.downloadHandler.text);

                    query.OnSuccess(request.downloadHandler.text);
                }
            }
        }
    }
}
