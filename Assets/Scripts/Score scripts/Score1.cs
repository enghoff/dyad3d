﻿using UnityEngine;

public class Score1 : Score
{
	public bool gameComplete = false;

    protected override void SetTotalScore()
    {
        base.SetTotalScore();

        if (overallScore >= 2)
        {
            Stats.SetFloat("Level1Score", levelscore);
            gameComplete = true;
        }
    }
}