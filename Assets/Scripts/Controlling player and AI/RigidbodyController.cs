﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidbodyController : MonoBehaviour
{
    public float maxVelocity = 5f;
    public float acceleration = 5f;

    protected Rigidbody _rigidbody;

    protected void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");		//this controls the horizontal movement of the ball
        float moveVertical = Input.GetAxis("Vertical");         //this controls the verical movement
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);		//this calculates the vector the ball should head in and stores it as a variable movement. 

        _rigidbody.AddForce(movement * acceleration * Time.deltaTime, ForceMode.VelocityChange);							//this applies force to the vector (movement)

        if (_rigidbody.velocity.magnitude > maxVelocity) _rigidbody.velocity = _rigidbody.velocity.normalized * maxVelocity;
    }
}
