﻿using UnityEngine;
using UnityEngine.UI;

//This script is loads the Player ID set in the original script, on window 1, 'PlayerName'

public class Load_player_name : MonoBehaviour
{
	public Text Playername_Text;
	public string Player_ID;

	// Use this for initialization
	void Start ()
    {
		Player_ID = Stats.GetString("PlayerName", "failed to load");

		Playername_Text.text = "Player " + (Player_ID);
	}
}
