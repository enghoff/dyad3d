﻿using System.Collections.Generic;

public static class Stats
{
    private static Dictionary<string, object> _data = new Dictionary<string, object>();

    public static void SetString(string key, string value)
    {
        _data[key] = value;
    }

    public static void SetInt(string key, int value)
    {
        _data[key] = value;
    }

    public static void SetFloat(string key, float value)
    {
        _data[key] = value;
    }

    public static string GetString(string key, string defaultValue = "")
    {
        return (_data.ContainsKey(key)) ? (string)_data[key] : defaultValue;
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        return (_data.ContainsKey(key)) ? (int)_data[key] : defaultValue;
    }

    public static float GetFloat(string key, float defaultValue = 0f)
    {
        return (_data.ContainsKey(key)) ? (float)_data[key] : defaultValue;
    }
}
