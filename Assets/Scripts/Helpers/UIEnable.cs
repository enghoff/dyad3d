﻿using UnityEngine;
using UnityEngine.Events;

public class UIEnable : MonoBehaviour
{
    public UnityEvent OnEnabled;

    private void OnEnable()
    {
        OnEnabled.Invoke();
    }
}
