﻿using UnityEngine;
using UnityEngine.UI;

public class calculator12 : MonoBehaviour
{
    public Text L2score;
    public float L1finalscore;
    public float L2finalscore;
    public float calculation;

    // Use this for initialization
    void Start()
    {
        L1finalscore = Stats.GetFloat("Level1Score");
        L2finalscore = Stats.GetFloat("Level2Score");

        calculation = L1finalscore + L2finalscore;

        L2score.text = "" + (calculation);
    }
}
