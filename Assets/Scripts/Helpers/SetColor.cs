﻿using UnityEngine;
using UnityEngine.UI;

namespace CoupleGame
{
    [RequireComponent(typeof(Text))]
    public class SetColor : MonoBehaviour
    {
        public Color OnColor;
        public Color OffColor;

        public void Set(bool state)
        {
            GetComponent<Text>().color = (state) ? OnColor : OffColor;
        }
    }
}