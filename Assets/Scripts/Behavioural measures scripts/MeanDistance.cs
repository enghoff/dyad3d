﻿using UnityEngine;

public class MeanDistance : MonoBehaviour
{
    public Transform Player;
    public Transform AI;
    public GameObject Prison;

    private float _meanDistance;
    private float _startAt;

    private void Start()
    {
        _startAt = Time.time;
    }

    private void Update()
    {
        if (Player.gameObject.activeInHierarchy && AI.gameObject.activeInHierarchy && !Prison.activeInHierarchy)
        {
            _meanDistance += Time.deltaTime / (Time.deltaTime + Time.time - _startAt) * ((Player.position - AI.position).magnitude - _meanDistance);

            LevelManager.SetStat("SpaceCoordination_Level{0}", _meanDistance);
        }
    }
}
