﻿using UnityEngine;
using UnityEngine.Events;

using System.IO;

using LitJson;
using Crosstales.FB;

public static class Config
{
    private static Model.User _user;
    public static Model.User user
    {
        get { return _user; }
        set
        {
            _user = value;
            config = value.config;
        }
    }

    public static Model.Config config { get; private set; }

    public static void LoadLocal()
    {
        config = JsonMapper.ToObject<Model.Config>(PlayerPrefs.GetString("config", "{}"));
    }

    public static void Commit(bool showSpinner = true)
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            user.config = config;

            WebController.Post<Model.User, Model.Null>("/dyad3d.asmx/config", user, showSpinner: showSpinner);
        }
        else PlayerPrefs.SetString("config", JsonMapper.ToJson(config));
    }

    public static void StartGame(string guid, UnityAction<Model.Config> onComplete, UnityAction onNotFound)
    {
        WebController.Post<Model.Id, Model.UserResponse>("/dyad3d.asmx/game", new Model.Id() { Guid = guid },
                    user => { Config.user = user.d; onComplete.Invoke(config); },
                    code => onNotFound.Invoke());
    }

    public static string FormatDefault(this string value, string defaultValue)
    {
        return (!string.IsNullOrEmpty(value)) ? value : defaultValue;
    }

    public static void Load()
    { 
        var path = FileBrowser.OpenSingleFile("Open Config", Application.dataPath, new ExtensionFilter[] { new ExtensionFilter("JSON", "json") });

        if (File.Exists(path)) config = JsonMapper.ToObject<Model.Config>(File.ReadAllText(path));
    }

    public static void Save()
    {
        var path = FileBrowser.SaveFile("Save Config as", Application.dataPath, "config", new ExtensionFilter[] { new ExtensionFilter("JSON", "json") });

        if (!string.IsNullOrEmpty(path)) File.WriteAllText(path, JsonMapper.ToJson(config));
    }
}
