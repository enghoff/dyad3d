﻿using UnityEngine;
using UnityEngine.UI;

public class ScorecalculatorFinal : MonoBehaviour
{
    public Text total;
    public float s1;
    public float s2;
    public float s3;
    public float s4;
    public float calculation;

    public Text dyad1;
    public Text dyad2;
    public Text dyad3;
    public Text dyad4;
    public Text dyad5;
    public Text dyad6;
    public Text dyad7;
    public Text dyad8;
    public Text dyad9;

    public float a;
    public float b;
    public float c;
    public float d;
    public float e;
    public float f;
    public float g;
    public float h;
    public float i;

    // Use this for initialization
    void Start()
    {
        s1 = Stats.GetFloat("Level1Score");
        s2 = Stats.GetFloat("Level2Score");
        s3 = Stats.GetFloat("Level3Score");
        s4 = Stats.GetFloat("Level4Score");

        calculation = s1 + s2 + s3 + s4;

        total.text = "" + (calculation);

        a = 1223 + calculation;
        dyad1.text = "" + (a);

        b = 1140 + calculation;
        dyad2.text = "" + (b);

        c = 1048 + calculation;
        dyad3.text = "" + (c);

        d = 1027 + calculation;
        dyad4.text = "" + (d);

        e = 795 + calculation;
        dyad5.text = "" + (e);

        f = 742 + calculation;
        dyad6.text = "" + (f);

        g = 691 + calculation;
        dyad7.text = "" + (g);

        h = 658 + calculation;
        dyad8.text = "" + (h);

        i = 512 + calculation;
        dyad9.text = "" + (i);
    }
}
