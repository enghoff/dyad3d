﻿using System.Collections.Generic;

using LitJson;

public class Model
{
    public class Id
    {
        public string Guid = string.Empty;
    }

    public class User : Id
    {
        public Config config
        {
            get { return JsonMapper.ToObject<Config>(Config);}
            set { Config = JsonMapper.ToJson(value); }
        }

        public string Username = string.Empty;
        public string Password = string.Empty;
        public string Config = "{}";
        public int Id;
    }

    public class DataPoint : Id
    {
        public string Header;
        public string Data;
    }

    public class UserResponse
    {
        public User d;
    }

    public class Scene
    {
        public string Name;
        public bool Active = true;
    }

    public class Config
    {
        public string AIID;
        public string AIDesc;
        public bool DynamicComms;
        public bool DisplayInfoInGame;
        public bool DisplayOnlineInfo;
        public string AImessage1;
        public string AImessage2;
        public string AImessage3;
        public string AImessage4;
//        public string AImessage5;
//        public string AImessage6;
//        public string AImessage7;
//        public string AImessage8;
        public int AI1typedelay;
        public int AI2typedelay;
        public int AI3typedelay;
        public int AI4typedelay;
        public int UO;
        public int IU;
        public int GF;
        public int HS;
        public string Intro1;
        public string Intro2;
        public string Dyad1A;
        public string Dyad1B;
        public string Dyad2A;
        public string Dyad2B;
        public string Dyad3A;
        public string Dyad3B;
        public string Dyad4A;
        public string Dyad4B;
        public string Dyad5A;
        public string Dyad5B;
        public string Dyad6A;
        public string Dyad6B;
        public string Dyad7A;
        public string Dyad7B;
        public string Dyad8A;
        public string Dyad8B;
        public string Dyad9A;
        public string Dyad9B;

        public List<Scene> Scenes = new List<Scene>();
    }

    public class Null { }
}
