﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{
	public new InputField name;
	public string PlayerID;
	public Text submittedText;
	public GameObject Nextbutton;

	public void setget()
	{
		PlayerID = name.text;
		Stats.SetString ("PlayerName", PlayerID);
		submittedText.text="Player "+name.text;
		Nextbutton.SetActive (true);
	}
}