﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

//This script controls the tutorial level mechanics including monitoring the score (i.e. has the gold cube been picked up?), destroying the player if this is true, and making the next level button active if so.

public class Tutorial1 : MonoBehaviour
{
    PlayerController _shortcuttoP1score;
    AIController _shortcuttoAIscore;
    public int overallScore;
    public Text totalScore;
    public int P1score;
    public GameObject NextLevel;
    public GameObject Playerplayer;
    public GameObject PlayerBlast;

    public int delay = 2;


    void Start()
    {
        _shortcuttoP1score = gameObject.GetComponentInChildren<PlayerController>();
        SetTotalScore();
        NextLevel.SetActive(false);
    }

    void Update()
    {
        P1score = _shortcuttoP1score.count;
        overallScore = P1score;
        SetTotalScore();
    }

    void SetTotalScore()
    {
        totalScore.text = "Total Score: " + overallScore.ToString();
        if (P1score > 3)
        {
            StartCoroutine(EndGame());                      //if the gold cube has been picked up then the coroutine must start
        }
    }

    IEnumerator EndGame()                                       //coroutines have to execute everything in sequential order. This is to prevent the user having any more control on the level once it is completed.
    {
        PlayerBlast.SetActive(true);
        yield return new WaitForSecondsRealtime(delay);
        Playerplayer.SetActive(false);
        NextLevel.SetActive(true);
    }
}