﻿using UnityEngine;
using UnityEngine.UI;

public class Loadtogglechoice : MonoBehaviour
{
    public Text personality;
    public string x;

    // Use this for initialization
    void Start()
    {
        x = Stats.GetString("Personality", "failed to load");

        personality.text = "" + (x);
    }
}
