﻿using UnityEngine;
using UnityEngine.UI;

public class LoadIntro1 : MonoBehaviour
{
	public Text Intro1Display;
	public string Intro1Load;

	void Start ()
    {
		Intro1Load = Config.config.Intro1.FormatDefault("failed to load");
        Intro1Display.text = Intro1Load;
	}
}
