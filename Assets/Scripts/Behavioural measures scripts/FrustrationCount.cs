﻿using UnityEngine;

using System.Collections.Generic;

public class FrustrationCount : MonoBehaviour
{
    private readonly KeyCode[] _keyCodes = new KeyCode[] { KeyCode.DownArrow, KeyCode.UpArrow, KeyCode.LeftArrow, KeyCode.RightArrow };

    private Dictionary<KeyCode, bool> _keyDown = new Dictionary<KeyCode, bool>();
    private Dictionary<KeyCode, float> _keyDownAt = new Dictionary<KeyCode, float>();
    private int _keyDownCount;
    private float _keyDownMean;

    private void Start()
    {
        foreach (var key in _keyCodes)
        {
            _keyDown.Add(key, false);
            _keyDownAt.Add(key, float.NaN);
        }

        LevelManager.SetStat("PrisonButtonBash_Level3", _keyDownCount);
        LevelManager.SetStat("PrisonPressDuration_Level3", _keyDownMean);
    }

    private void Update()
    {
        foreach (var key in _keyCodes)
        {
            if (Input.GetKey(key) && !_keyDown[key]) _keyDownAt[key] = Time.time;
            if (!Input.GetKey(key) && _keyDown[key])
            {
                _keyDownMean += (Time.time - _keyDownAt[key] - _keyDownMean) / (++_keyDownCount);

                LevelManager.SetStat("PrisonButtonBash_Level3", _keyDownCount);
                LevelManager.SetStat("PrisonPressDuration_Level3", _keyDownMean);
            }

            _keyDown[key] = Input.GetKey(key);
        }
    }
}