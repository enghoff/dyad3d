﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using System;
using System.Linq;

public class LaunchController : MonoBehaviour
{
    public string Query;
	public GameObject Admin;
	public GameObject CommitCSV;
	public SceneField DefaultScene;
    public SceneField ConfigScene;
    public UnityEvent OnInvalidGame;

    private void Start ()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (!string.IsNullOrEmpty(Application.absoluteURL))
            {
                Query = (new Uri(Application.absoluteURL)).Query.ToLower().TrimStart('?');
            }

            if (Query == "admin") Admin.SetActive(true);
            else
            {
                Config.StartGame(Query, config =>
                {
                    var scene = config.Scenes.FirstOrDefault(s => s.Active);
                    SceneManager.LoadScene((scene != null) ? scene.Name : DefaultScene.SceneName);
                    //CommitCSV.SetActive(true);
                },
                () => OnInvalidGame.Invoke());
            }
        }
        else
        {
            Config.LoadLocal();
            var scene = Config.config.Scenes.FirstOrDefault(s => s.Active);
            SceneManager.LoadScene((scene != null) ? scene.Name : DefaultScene.SceneName);
            //CommitCSV.SetActive(true);
        }
    }

    public void OnAuthenticated()
    {
        SceneManager.LoadScene(ConfigScene.SceneName);
    }
}
