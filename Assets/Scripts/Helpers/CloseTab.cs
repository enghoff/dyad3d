﻿using UnityEngine;
using System.Runtime.InteropServices;

public class CloseTab : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void closeWindow();

    public void Close()
    {
#if !UNITY_EDITOR
		closeWindow();
#endif
    }
}
