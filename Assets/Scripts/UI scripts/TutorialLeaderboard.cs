﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialLeaderboard : MonoBehaviour
{
    public Text tscore;
    public float tfinalscore;

    // Use this for initialization
    void Start()
    {
        tfinalscore = Stats.GetFloat("TutorialScore");

        tscore.text = "" + (tfinalscore);
    }
}
