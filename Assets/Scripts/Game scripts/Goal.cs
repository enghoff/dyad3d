﻿using UnityEngine;

using System.Collections.Generic;

public class Goal : MonoBehaviour
{
    public enum AgentType { Player, AI }
    public AgentType Agent;

    private static Dictionary<AgentType, float> _pickedAt = new Dictionary<AgentType, float>();

    private void Start()
    {
        _pickedAt.Clear();

        LevelManager.SetStat("CubeCoordination_Level{0}", float.NaN);
    }

    private void OnDisable()
    {
        _pickedAt[Agent] = Time.time;

        if (_pickedAt.ContainsKey(AgentType.AI) && _pickedAt.ContainsKey(AgentType.Player))
        {
            LevelManager.SetStat("CubeCoordination_Level{0}", _pickedAt[AgentType.AI] - _pickedAt[AgentType.Player]);
        }
    }
}
