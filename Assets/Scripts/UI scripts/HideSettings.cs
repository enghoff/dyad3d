﻿using UnityEngine;

public class HideSettings : MonoBehaviour
{
    public GameObject SettingsOption;

    void Update()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer) return;

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SettingsOption.SetActive(true);
            Debug.Log("button 6 pressed to reveal settings");
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            SettingsOption.SetActive(false);
            Debug.Log("button 7 pressed to hide settings");
        }
    }
}
