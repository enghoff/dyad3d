﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardScript2 : LeaderboardScript
{
    public Text L2score;
    public float L2finalscore;
    public Text AImessageL2;
    public string AImessage;
	public int typingdelayL2;
    public string PlayerMessage2;
    public InputField PlayerInputMessage2;
	public GameObject Tick;

    // Use this for initialization
    void Start()
    {
        L2finalscore = Stats.GetFloat("Level2Score");
        L2score.text = "" + (L2finalscore);
        typingdelayL2 = Config.config.AI2typedelay;
        StartCoroutine(TypingDelay());
    }

    public void SavePlayerMessage2()
    {
        PlayerMessage2 = PlayerInputMessage2.text;
        Stats.SetString("PlayerMessage2", PlayerMessage2);
    }

    IEnumerator TypingDelay()
    {
		yield return new WaitForSecondsRealtime(typingdelayL2);
        Debug.Log("coroutine typing delay active");
        AImessage = Config.config.AImessage2;
        Tick.SetActive (true);
        AImessageL2.text = (AImessage);
    }
}