﻿using UnityEngine;
using UnityEngine.UI;

public class calculator34 : MonoBehaviour
{
    public Text L4score;
    public float L1finalscore;
    public float L2finalscore;
    public float L3finalscore;
    public float L4finalscore;
    public float calculation;

    // Use this for initialization
    void Start()
    {
        L1finalscore = Stats.GetFloat("Level1Score");
        L2finalscore = Stats.GetFloat("Level2Score");
        L3finalscore = Stats.GetFloat("Level3Score");
        L4finalscore = Stats.GetFloat("Level4Score");

        calculation = L1finalscore + L2finalscore + L3finalscore + L4finalscore;

        L4score.text = "" + (calculation);
    }
}
