﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardScript4 : LeaderboardScript
{
    public Text L4score;
    public float L4finalscore;
    public Text AImessageL4;
    public string AImessage;
	public int typingdelayL4;
    public string PlayerMessage4;
    public InputField PlayerInputMessage4;
	public GameObject Tick;

    // Use this for initialization
    void Start()
    {
        L4finalscore = Stats.GetFloat("Level4Score");
        L4score.text = "" + (L4finalscore);
        typingdelayL4 = Config.config.AI4typedelay;

        StartCoroutine(TypingDelay());
    }

    public void SavePlayerMessage4()
    {
        PlayerMessage4 = PlayerInputMessage4.text;
        Stats.SetString("PlayerMessage4", PlayerMessage4);
    }

    IEnumerator TypingDelay()
    {
		yield return new WaitForSecondsRealtime(typingdelayL4);
        Debug.Log("coroutine typing delay active");
        AImessage = Config.config.AImessage4;
        Tick.SetActive (true);
        AImessageL4.text = (AImessage);
    }
}
