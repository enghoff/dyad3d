﻿using UnityEngine;

public class Prison_script_Player : MonoBehaviour
{
    public GameObject door, b1;

    void Start()
    {
        door.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "AI")
        {
            door.gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "AI")
        {
            door.gameObject.SetActive(false);
        }
    }
}


