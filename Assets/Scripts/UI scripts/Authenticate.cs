﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using System.Text;
using System.Security.Cryptography;

public class Authenticate : MonoBehaviour
{
    public InputField Username;
    public InputField Password;

    public UnityEvent OnLoginSuccess;
    public UnityEvent OnLoginFail;
    public UnityEvent OnRegistrationSuccess;
    public UnityEvent OnRegistrationFail;
    public UnityEvent OnValidationFail;

    public void OnLogin()
    {
        if (Validate())
        {
            WebController.Post<Model.User, Model.UserResponse>("/dyad3d.asmx/authenticate",
                new Model.User() { Username = Username.text.ToLower(), Password = Hash(Password.text) },
                user => { Config.user = user.d; OnLoginSuccess.Invoke(); },
                nil => OnLoginFail.Invoke());
        }
    }

    public void OnRegister()
    {
        if (Validate())
        {
            WebController.Post<Model.User, Model.UserResponse>("/dyad3d.asmx/register", 
                new Model.User() { Username = Username.text.ToLower(), Password = Hash(Password.text) }, 
                nil => OnRegistrationSuccess.Invoke(), 
                nil => OnRegistrationFail.Invoke());
        }
    }

    private bool Validate()
    {
        var success = (Username.text.Length >= 3 && Password.text.Length >= 3);

        if (!success) OnValidationFail.Invoke();

        return success;
    }

    private static string Hash(string password)
    {
        var md5 = new MD5CryptoServiceProvider();
        var data = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
        var hash = string.Empty;

        foreach (var b in data) hash += string.Format("{0:x2}", b);

        return hash;
    }
}
