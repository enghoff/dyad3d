﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIController : MonoBehaviour
{
    public int collisionCount;

    void Start()
    {
        collisionCount = 0;   
    }
   
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up AI"))
        {
            other.gameObject.SetActive(false);
            collisionCount = collisionCount + 1;
        }
    }
}