﻿using UnityEngine;
using UnityEngine.UI;

public class AIdesc : MonoBehaviour
{
    public Text Playername;

    public string x;

    // Use this for initialization
    void Start()
    {
        x = Config.config.AIDesc.FormatDefault("failed to load");

        Playername.text = "" + (x);
    }
}
