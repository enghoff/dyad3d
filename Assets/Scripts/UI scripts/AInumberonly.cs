﻿using UnityEngine;
using UnityEngine.UI;

public class AInumberonly : MonoBehaviour
{
    public Text Playername;
    public string AI_ID;

    void Start()
    {
        AI_ID = Config.config.AIID.FormatDefault("failed to load");
        Playername.text = "" + (AI_ID);
    }   
}
