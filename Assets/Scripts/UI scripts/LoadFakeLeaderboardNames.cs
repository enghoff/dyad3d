﻿using UnityEngine;
using UnityEngine.UI;

public class LoadFakeLeaderboardNames : MonoBehaviour
{
	public Text D1A;
	private string dy1A;
	public Text D1B;
	private string dy1B;

	public Text D2A;
	private string dy2A;
	public Text D2B;
	private string dy2B;

	public Text D3A;
	private string dy3A;
	public Text D3B;
	private string dy3B;

	public Text D4A;
	private string dy4A;
	public Text D4B;
	private string dy4B;

	public Text D5A;
	private string dy5A;
	public Text D5B;
	private string dy5B;

	public Text D6A;
	private string dy6A;
	public Text D6B;
	private string dy6B;

	public Text D7A;
	private string dy7A;
	public Text D7B;
	private string dy7B;

	public Text D8A;
	private string dy8A;
	public Text D8B;
	private string dy8B;

	public Text D9A;
	private string dy9A;
	public Text D9B;
	private string dy9B;

	void Start ()
	{
		dy1A = Config.config.Dyad1A.FormatDefault("failed to load");
		D1A.text = dy1A;

        dy1B = Config.config.Dyad1B.FormatDefault("failed to load");
		D1B.text = dy1B;

		dy2A = Config.config.Dyad2A.FormatDefault("failed to load");
        D2A.text = dy2A;

		dy2B = Config.config.Dyad2B.FormatDefault("failed to load");
        D2B.text = dy2B;
    
		dy3A = Config.config.Dyad3A.FormatDefault("failed to load");
        D3A.text = dy3A;

		dy3B = Config.config.Dyad3B.FormatDefault("failed to load");
        D3B.text = dy3B;

		dy4A = Config.config.Dyad4A.FormatDefault("failed to load");
        D4A.text = dy4A;

		dy4B = Config.config.Dyad4B.FormatDefault("failed to load");
        D4B.text = dy4B;

		dy5A = Config.config.Dyad5A.FormatDefault("failed to load");
        D5A.text = dy5A;

		dy5B = Config.config.Dyad5B.FormatDefault("failed to load");
        D5B.text = dy5B;

		dy6A = Config.config.Dyad6A.FormatDefault("failed to load");
        D6A.text = dy6A;

		dy6B = Config.config.Dyad6B.FormatDefault("failed to load");
        D6B.text = dy6B;

		dy7A = Config.config.Dyad7A.FormatDefault("failed to load");
        D7A.text = dy7A;

		dy7B = Config.config.Dyad7B.FormatDefault("failed to load");
        D7B.text = dy7B;

		dy8A = Config.config.Dyad8A.FormatDefault("failed to load");
        D8A.text = dy8A;

		dy8B = Config.config.Dyad8B.FormatDefault("failed to load");
        D8B.text = dy8B;

		dy9A = Config.config.Dyad9A.FormatDefault("failed to load");
        D9A.text = dy9A;

		dy9B = Config.config.Dyad9B.FormatDefault("failed to load");
        D9B.text = dy9B;
	}
}
