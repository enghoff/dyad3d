﻿using UnityEngine;
using System.Collections;

public class Prison_script_AI : MonoBehaviour
{
    public GameObject door, b1;

    void Start()
    {
        door.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            door.gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            door.gameObject.SetActive(false);
        }
    }
}


