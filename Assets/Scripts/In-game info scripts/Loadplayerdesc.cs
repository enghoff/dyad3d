﻿using UnityEngine;
using UnityEngine.UI;

public class Loadplayerdesc: MonoBehaviour
{
	public Text Playerdesc;
	public string x;

	// Use this for initialization
	void Start ()
    {
		x = Stats.GetString("Playerdesc", "failed to load");

		Playerdesc.text = "" + (x);
	}
}
