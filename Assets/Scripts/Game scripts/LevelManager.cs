﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public int Level;

    private static LevelManager _instance;

    private void Awake()
    {
        _instance = this;
    }

    public static int? CurrentLevel
    {
        get
        {
            return (_instance != null) ? _instance.Level : (int?)null;
        }
    }

    public static void SetStat(string key, int value)
    {
        Stats.SetInt(string.Format(key, CurrentLevel), value);
    }

    public static void SetStat(string key, float value)
    {
        Stats.SetFloat(string.Format(key, CurrentLevel), value);
    }

    public void WriteCSV()
    {
        CSVWriter.CommitData();
    }
}
