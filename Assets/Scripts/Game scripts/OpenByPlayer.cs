﻿using UnityEngine;

//This is the script that allows the players to open doors and prisons

public class OpenByPlayer : MonoBehaviour
{
    public GameObject door;

    void Start()
    {
        door.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")         //tests for the condition that the object coliding with the door has the tage "player" so that only the Player, and not the AI, can open it.
        {
            door.gameObject.SetActive(false);
        }
    }
}


