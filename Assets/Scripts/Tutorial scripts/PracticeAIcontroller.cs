﻿using UnityEngine;
using UnityEngine.UI;

// This cript is a mirror image of the Player Controller and is only used in tutorial pahse where you are able to control the silver ball. 
// In the real game the silver ball is controlled by the Patrol script.

public class PracticeAIcontroller : RigidbodyController
{
    public Text countText;
    public Text winText;
    public int count;

    void Start()
    {
        count = 0;
        SetCountText();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up AI"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(true);
        }
    }

    void OnTriggerEnter2(Collider button)
    {
        if (button.gameObject.CompareTag("Button"))
        {
            button.gameObject.SetActive(true);
        }
    }

    void SetCountText()
    {
        countText.text = "P1 Score: " + count.ToString();
    }
}