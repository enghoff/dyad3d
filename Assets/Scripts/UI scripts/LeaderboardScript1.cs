﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardScript1 : LeaderboardScript
{
    public Text L1score;
    public float L1finalscore;
    public Text AImessageL1;
	public string AImessage;
	public int typingdelayL1;
    public string PlayerMessage1;
    public InputField PlayerInputMessage1;
	public GameObject tick;

    // Use this for initialization
    void Start()
    {
        L1finalscore = Stats.GetFloat("Level1Score");
        L1score.text = "" + (L1finalscore);
        typingdelayL1 = Config.config.AI1typedelay;
        StartCoroutine(TypingDelay());
    }

    public void SavePlayerMessage1()
    {
        PlayerMessage1 = PlayerInputMessage1.text;
        Stats.SetString("PlayerMessage1", PlayerMessage1);
    }

    IEnumerator TypingDelay()
    {
		yield return new WaitForSecondsRealtime(typingdelayL1);
        Debug.Log("coroutine typing delay active");
        AImessage = Config.config.AImessage1;
        AImessageL1.text = (AImessage);
		tick.SetActive (true);
    }
}