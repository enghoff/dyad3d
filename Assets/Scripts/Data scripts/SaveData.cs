﻿using System;



[Serializable]

public class SaveData
{
    public string name;
    public string condition;
    public string player_info_shared;
    public string personality;
    public float Tutorialscore;
    public float L1score;
    public float L2score;
    public float L3score;
    public float L4score;
    public float scoretotal;
    public int frustration;
    public string priorities;
    public float revenge;
    public float hesitation;
    public float primarysearch;
    public float secondarysearch;
}
