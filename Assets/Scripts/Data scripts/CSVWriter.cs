﻿using UnityEngine;

using System;
using System.IO;
using System.Collections.Generic;

public static class CSVWriter
{
    public static void CommitData()
    {
        var data = new Dictionary<string, object>()
        {
            { "ID", Stats.GetString("PlayerName") },
            { "Stimulus", Config.config.AIDesc.FormatDefault("NO DATA") },
            { "Info_shared", Stats.GetString("Playerdesc", "NO DATA") },
            { "Personality", Stats.GetString("Personality", "NO DATA") },
            { "tutorial_score", Stats.GetFloat("TutorialScore", -1) },
            { "Level1Score", Stats.GetFloat("Level1Score", -1) },
            { "Level2Score", Stats.GetFloat("Level2Score", -1) },
            { "Level3Score", Stats.GetFloat("Level3Score", -1) },
            { "Level4Score", Stats.GetFloat("Level4Score", -1) },
            { "PrisonButtonBash_Level3", Stats.GetInt("PrisonButtonBash_Level3") },
            { "UnlockingPrisonToFreeingFromPrisonTime_Level4", Stats.GetFloat("UnlockingPrisonToFreeingFromPrisonTime_Level4") },
            { "MiddleMapHesitation_Level4", Stats.GetFloat("MiddleMapHesitation_Level4") },
            { "PrimarySearchZoneDuration_Level4", Stats.GetFloat("PrimarySearchZoneDuration_Level4") },
            { "SecondarySearchZoneDuration_Level4", Stats.GetFloat("SecondarySearchZoneDuration_Level4") },
            { "Priorities_Level4", Stats.GetString("Priorities_Level4", "NO DATA") },
            { "PrisonPressDuration_Level3", Stats.GetFloat("PrisonPressDuration_Level3", float.NaN) },

            { "BumpCount_Level1", Stats.GetInt("BumpCount_Level1", -1) },
            { "CubeCoordination_Level1", Stats.GetFloat("CubeCoordination_Level1", float.NaN) },
            { "SpaceCoordination_Level1", Stats.GetFloat("SpaceCoordination_Level1", float.NaN) },
            { "ButtonBash_Level1", Stats.GetInt("ButtonBash_Level1", -1) },
            { "MeanButtonPressDuration_Level1", Stats.GetFloat("MeanButtonPressDuration_Level1", float.NaN) },

            { "BumpCount_Level2", Stats.GetInt("BumpCount_Level2", -1) },
            { "CubeCoordination_Level2", Stats.GetFloat("CubeCoordination_Level2", float.NaN) },
            { "SpaceCoordination_Level2", Stats.GetFloat("SpaceCoordination_Level2", float.NaN) },
            { "ButtonBash_Level2", Stats.GetInt("ButtonBash_Level2", -1) },
            { "MeanButtonPressDuration_Level2", Stats.GetFloat("MeanButtonPressDuration_Level2", float.NaN) },

            { "BumpCount_Level3", Stats.GetInt("BumpCount_Level3", -1) },
            { "CubeCoordination_Level3", Stats.GetFloat("CubeCoordination_Level3", float.NaN) },
            { "SpaceCoordination_Level3", Stats.GetFloat("SpaceCoordination_Level3", float.NaN) },
            { "ButtonBash_Level3", Stats.GetInt("ButtonBash_Level3", -1) },
            { "MeanButtonPressDuration_Level3", Stats.GetFloat("MeanButtonPressDuration_Level3", float.NaN) },

            { "BumpCount_Level4", Stats.GetInt("BumpCount_Level4", -1) },
            { "CubeCoordination_Level4", Stats.GetFloat("CubeCoordination_Level4", float.NaN) },
            { "SpaceCoordination_Level4", Stats.GetFloat("SpaceCoordination_Level4", float.NaN) },
            { "ButtonBash_Level4", Stats.GetInt("ButtonBash_Level4", -1) },
            { "MeanButtonPressDuration_Level4", Stats.GetFloat("MeanButtonPressDuration_Level4", float.NaN) },

            { "AIID", Config.config.AIID },
            { "DynamicComms", Config.config.DynamicComms ? "True" : "False" },
			{ "DisplayInfoInGame", Config.config.DisplayInfoInGame ? "True" : "False" },
			{ "DisplayOnlineInfo", Config.config.DisplayOnlineInfo ? "True" : "False" },
            { "AImessage1", Config.config.AImessage1 },
            { "AImessage2", Config.config.AImessage2 },
            { "AImessage3", Config.config.AImessage3 },
            { "AImessage4", Config.config.AImessage4 },
            { "PlayerMessage1", Stats.GetString("PlayerMessage1") },
			{ "PlayerMessage2", Stats.GetString("PlayerMessage2") },
			{ "PlayerMessage3", Stats.GetString("PlayerMessage3") },
			{ "PlayerMessage4", Stats.GetString("PlayerMessage4") },
            { "Intro1", Config.config.Intro1 },
            { "Intro2", Config.config.Intro2 },
            { "Dyad1A", Config.config.Dyad1A },
            { "Dyad1B", Config.config.Dyad1B },
            { "Dyad2A", Config.config.Dyad2A },
            { "Dyad2B", Config.config.Dyad2B },
            { "Dyad3A", Config.config.Dyad3A },
            { "Dyad3B", Config.config.Dyad3B },
            { "Dyad4A", Config.config.Dyad4A },
            { "Dyad4B", Config.config.Dyad4B },
            { "Dyad5A", Config.config.Dyad5A },
            { "Dyad5B", Config.config.Dyad5B },
            { "Dyad6A", Config.config.Dyad6A },
            { "Dyad6B", Config.config.Dyad6B },
            { "Dyad7A", Config.config.Dyad7A },
            { "Dyad7B", Config.config.Dyad7B },
            { "Dyad8A", Config.config.Dyad8A },
            { "Dyad8B", Config.config.Dyad8B },
            { "Dyad9A", Config.config.Dyad9A },
            { "Dyad9B", Config.config.Dyad9B }
        };

        if (Application.platform == RuntimePlatform.WebGLPlayer) WebController.Post<Model.DataPoint, Model.Null>("/dyad3d.asmx/datapoint", new Model.DataPoint() { Guid = Config.user.Guid, Header = Header(data), Data = Row(data) });
        else FileWrite(data);
    }

    private static void FileWrite(Dictionary<string, object> data)
    {
        var path = Application.persistentDataPath + "\\data.csv";

        if (!File.Exists(path)) File.WriteAllText(path, Header(data));

        File.AppendAllText(path, Environment.NewLine + Row(data));
    }

    private static string Header(Dictionary<string, object> data)
    {
        var result = string.Empty;
        var i = 0;

        foreach (var entry in data)
        {
            result += "\"" + entry.Key + "\"" + ((++i < data.Count) ? "," : string.Empty);
        }

        return result;
    }

    private static string Row(Dictionary<string, object> data)
    {
        var result = string.Empty;
        var i = 0;

        foreach (var entry in data)
        {
            result += ((entry.Value is string) ? "\"" + entry.Value.ToString().Replace("\"", "\"\"") + "\"" : entry.Value) + ((++i < data.Count) ? "," : string.Empty);
        }

        return result;
    }
}
