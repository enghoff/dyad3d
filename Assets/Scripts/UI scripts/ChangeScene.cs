﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Linq;

public class ChangeScene : MonoBehaviour
{
    public void ChangeToScene(string sceneToChangeTo)
    {
        var scene = SceneManager.GetActiveScene();
        var scenes = Config.config.Scenes.Where(s => s.Active).ToList();
        var index = scenes.FindIndex(s => s.Name == scene.name) + 1;

        SceneManager.LoadScene((index > 0 && index < scenes.Count) ? scenes[index].Name : sceneToChangeTo);
    }

    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}