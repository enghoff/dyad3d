using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScriptLevel2 : MonoBehaviour
{

    public GameObject Nextbutton;
    public int wait = 6;
    private int seven = 7;
    private int eight = 8;
    private int nine = 9;
    private int ten = 10;

    public GameObject Loadingeffect;
    public GameObject Waitingtext;
    public GameObject Readytext;
    public Text threetwoone;

    public GameObject three;
    public GameObject two;
    public GameObject one;

    public float seconds, minutes;

    // Use this for initialization
    void Start()
    {
        Nextbutton.SetActive(false);
        Readytext.SetActive(false);
        wait = 6;
    }

    // Update is called once per frame
    void Update()
    {
        minutes = (int)(Time.timeSinceLevelLoad / 60f);
        seconds = (int)(Time.timeSinceLevelLoad % 60f);
        waitcount();
    }

    void waitcount()
    {
        if (seconds <= wait)
        {
            Nextbutton.SetActive(false);
            Readytext.SetActive(false);
            Loadingeffect.SetActive(true);
            Waitingtext.SetActive(true);
        }
        else if (seconds <= seven)
        {
            Loadingeffect.SetActive(false);
            Waitingtext.SetActive(false);
            three.SetActive(true);
        }
        else if (seconds <= eight)
        {
            three.SetActive(false);
            two.SetActive(true);
        }
        else if (seconds <= nine)
        {
            two.SetActive(false);
            one.SetActive(true);
        }
        else if (seconds <= ten)
        {
            one.SetActive(false);
            SceneManager.LoadScene("11BLevel2");
        }
    }
}