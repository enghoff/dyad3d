﻿using UnityEngine;
using UnityEngine.UI;

public class LoadIntro2 : MonoBehaviour
{
	public Text Intro2Display;
	public string Intro2Load;

	void Start ()
    {
		Intro2Load = Config.config.Intro2.FormatDefault("failed to load");
        Intro2Display.text = Intro2Load;	
	}
}
