﻿using UnityEngine;

public class Exit : MonoBehaviour
{
    public void ClickExit()
    {
        Application.Quit();
    }
}