﻿using UnityEngine;

//This script hides triggers so that they can be discovered to unlock doors and prisons in the game.
//it is attached to the Main_trigger game oject in either the door system or prison system hierarchy.
//along with the 'Door_open_by_**' script, which is attached to the 'Unlock_zone' game object in the system hierarchy, these two scripts complete the function of hiding triggers and removing doors for each player respectively.

public class Trigger_unlock_script : MonoBehaviour
{
    public GameObject unlock_zone;
    public Renderer main_trigger_mesh;
    public Renderer inner_trigger_mesh;
    public GameObject sparkle;

    void Start()
    {
        main_trigger_mesh.enabled = false;
        inner_trigger_mesh.enabled = false;

        unlock_zone.gameObject.SetActive(false);
        sparkle.gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "AI")
        {
            sparkle.gameObject.SetActive(true);

            main_trigger_mesh.enabled = true;
            inner_trigger_mesh.enabled = true;
            unlock_zone.gameObject.SetActive(true);
        }

        else if (col.gameObject.tag == "Player")
        {
            sparkle.gameObject.SetActive(true);
            main_trigger_mesh.enabled = true;
            inner_trigger_mesh.enabled = true;
            unlock_zone.gameObject.SetActive(true);
        }
    }
}