﻿using UnityEngine;
using UnityEngine.UI;

public class Saveplayerdesc : MonoBehaviour
{
    public InputField playerinput;
    public string playerdesc;
    public GameObject nextbutton;

    void Start()
    {
        nextbutton.SetActive(false);
    }

    // Use this for initialization
    public void setdesc()
    {
        playerdesc = playerinput.text;
        Stats.SetString("Playerdesc", playerdesc);
        nextbutton.SetActive(true);
    }
}
