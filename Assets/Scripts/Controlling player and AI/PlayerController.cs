﻿using UnityEngine;

using System.Collections.Generic;

// This script controls the player.
// Since it is used in multiple levels, there are some variables such as 'heistation', 'primarySearch' and 'secondarySearch' which do not apply to most levels. 

public class PlayerController : RigidbodyController
{
    public int count;
    public float hesitation = 0.0f;
    public int count2;
    public float primarySearch = 0.0f;
    public int count3;
    public float secondarySearch = 0.0f;
    public int count4;

    private readonly KeyCode[] _keyCodes = new KeyCode[] { KeyCode.DownArrow, KeyCode.UpArrow, KeyCode.LeftArrow, KeyCode.RightArrow };

    private Dictionary<KeyCode, bool> _keyDown = new Dictionary<KeyCode, bool>();
    private Dictionary<KeyCode, float> _keyDownAt = new Dictionary<KeyCode, float>();
    private int _bumpCount;
    private int _keyDownCount;
    private float _keyDownMean;

    void Start()
    {
        count = 0;

        foreach (var key in _keyCodes)
        {
            _keyDown.Add(key, false);
            _keyDownAt.Add(key, float.NaN);
        }

        LevelManager.SetStat("BumpCount_Level{0}", _bumpCount);
        LevelManager.SetStat("ButtonBash_Level{0}", _keyDownCount);
        LevelManager.SetStat("MeanButtonPressDuration_Level{0}", _keyDownMean);
    }

    new void Update()
    {
        base.Update();

        SetHesitation();

        foreach (var key in _keyCodes)
        {
            if (Input.GetKey(key) && !_keyDown[key]) _keyDownAt[key] = Time.time;
            if (!Input.GetKey(key) && _keyDown[key])
            {
                _keyDownMean += (Time.time - _keyDownAt[key] - _keyDownMean) / (++_keyDownCount);

                LevelManager.SetStat("ButtonBash_Level{0}", _keyDownCount);
                LevelManager.SetStat("MeanButtonPressDuration_Level{0}", _keyDownMean);
            }

            _keyDown[key] = Input.GetKey(key);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            Debug.Log("Cube collided with");
        }
        else if (other.gameObject.CompareTag("Button"))
        {
            Debug.Log("Button collision");
            count2 = 1;
        }
        else if (other.gameObject.CompareTag("Primary"))
        {
            Debug.Log("Primary search zone entered");
            count3 = 1;
        }
        else if (other.gameObject.CompareTag("Secondary"))
        {
            Debug.Log("Secondary search zone entered");
            count4 = 1;
        }
    }

    void SetHesitation()
    {
        if (count2 >= 1)
        {
            hesitation += Time.deltaTime * 1f;
        }
        else if (count3 >= 1)
        {
            primarySearch += Time.deltaTime * 1f;
        }
        else if (count4 >= 1)
        {
            secondarySearch += Time.deltaTime * 1f;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Button"))
        {
            Debug.Log("Button exit");
            count2 = 0;
        }
        else if (other.gameObject.CompareTag("Primary"))
        {
            count3 = 0;
            Debug.Log("Primary search zone exited");
        }
        else if (other.gameObject.CompareTag("Secondary"))
        {
            count4 = 0;
            Debug.Log("Secondary search zone exited");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "AI")
        {
            LevelManager.SetStat("BumpCount_Level{0}", ++_bumpCount);
        }
    }
}