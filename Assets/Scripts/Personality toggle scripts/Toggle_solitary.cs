﻿using UnityEngine;

/* This is one of 10 small toggle scripts.
 * They are attached to an empty game object called 'Togglescript' which is at the bottom of the 'Togglegroup' hierarchy. 
 * The function of each of these scripts is to toggle the checkmark and save the option selected by the user as a personality type. 
 * This information will be exported at the end of the game to the json file. 
 * In each option in the hierarchy the 'Togglescript' is attached to the 'On Value changed boolean, with the 'Toggle_change' function selected below.
 * This means that when a checkbox is clicked, the 'Toggle-change; function on the relevant script will be activated. 
 * Note - all the scripts use the same variable 'personality', and clicking an option immediately stores it as in game data(check debug console for illustration)
 * Thus repeated clicking will continue to overwrite the StatsController key.
 * Hence data is stored before submit button is clicked. Submit button only makes the NextButton active.
 * */

public class Toggle_solitary : MonoBehaviour
{
    public string personality;

    public void Toggle_change(bool newValue)    //This is the function that toggles the checkmark and initates the following:
    {
        personality = "solitary";                               //assigns value to the variable personality
        Stats.SetString("Personality", personality);      //This saves the variable personality as data stored in the game, (lookup PlayrePrefs). 
        Debug.Log(personality + " saved as personality type");  //Check in the debug console which option has been saved.
    }
}
