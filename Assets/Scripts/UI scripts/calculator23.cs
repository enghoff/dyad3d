﻿using UnityEngine;
using UnityEngine.UI;

public class calculator23 : MonoBehaviour
{
    public Text L3score;
    public float L1finalscore;
    public float L2finalscore;
    public float L3finalscore;
    public float calculation;

    // Use this for initialization
    void Start()
    {
        L1finalscore = Stats.GetFloat("Level1Score");
        L2finalscore = Stats.GetFloat("Level2Score");
        L3finalscore = Stats.GetFloat("Level3Score");

        calculation = L1finalscore + L2finalscore + L3finalscore;

        L3score.text = "" + (calculation);
    }
}
