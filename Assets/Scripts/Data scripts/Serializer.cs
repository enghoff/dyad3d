﻿using System.IO;
using UnityEngine;



public class Serializer : MonoBehaviour
{
    
    public string SAVE_FILE = "player.json";
    public string id;
    public float total1;
    public float total2;
    public float total3;
    public float finaltotal;
    public string explanation;
    public string type;
    public float tutorial;
    public int irritation;
    public string prosocial;
    public float L4hesi;
    public float L4prim;
    public float L4second;
    public float total4;
    public float revengetime;
    public string group;

    void Start()
    {
   

        id = Stats.GetString("PlayerName");
        total1 = Stats.GetFloat("Level1Score");
        total2 = Stats.GetFloat("Level2Score");
        total3 = Stats.GetFloat("Level3Score");
        total4 = Stats.GetFloat("Level4Score");

        finaltotal = total1 + total2 + total3 + total4;

        explanation = Stats.GetString("Playerdesc", "NO DATA");

        type = Stats.GetString("Personality", "NO DATA");

        tutorial = Stats.GetFloat("TutorialScore");

        group = Config.config.AIDesc.FormatDefault("NO DATA");

        irritation = Stats.GetInt("PrisonButtonBash_Level3");

        prosocial = Stats.GetString("Priorities_Level4", "NO DATA");

        L4hesi = Stats.GetFloat("MiddleMapHesitation_Level4");

        L4prim = Stats.GetFloat("PrimarySearchZoneDuration_Level4");

        L4second = Stats.GetFloat("SecondarySearchZoneDuration_Level4");

        revengetime = Stats.GetFloat("UnlockingPrisonToFreeingFromPrisonTime_Level4");

        SAVE_FILE = "player" + id;

     

        SaveData data = new SaveData() { name = id, condition = group, player_info_shared = explanation, personality = type, Tutorialscore = tutorial, L1score = total1, L2score = total2, L3score = total3, L4score = total4, scoretotal = finaltotal, frustration = irritation, priorities = prosocial, revenge = revengetime, hesitation = L4hesi, primarysearch = L4prim, secondarysearch = L4second };

        string json = JsonUtility.ToJson(data);

        Debug.Log(json);


        string filename = Path.Combine(Application.persistentDataPath, SAVE_FILE);

        if (File.Exists(filename))
        {
            File.Delete(filename);
        }

        File.WriteAllText(filename, json);

        Debug.Log("player saved to " + filename);       
    }
}
